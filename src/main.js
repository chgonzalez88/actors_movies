// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.


//define the components
//import the vue instance
import Vue from 'vue'
//import the App component
import App from './App'
//import the vue router
import VueRouter from 'vue-router'
//import the hello component
import Hello from './components/Hello'

//import the hello component
import Home from './components/home'

//import the about component
import About from './components/About'

//import the about component
import ActorCreate from './components/actors/create'
//import the about component
import ActorUpdate from './components/actors/update'
//import the about component
import ActorIndex from './components/actors/index'

//import the about component
import MovieCreate from './components/movies/create'
//import the about component
import MovieUpdate from './components/movies/update'
//import the about component
import MovieIndex from './components/movies/index'


//tell vue to use the router
Vue.use(VueRouter)

//define the routes
const routes = [
	//define the root url of the application.
	{ path: '/', component: Home },
	//route for the about route of the web page
	{ path: '/about', component: About },

  //route for the about route of the web page
  { path: '/actors', component: ActorIndex },
  //route for the about route of the web page
  { path: '/actors/create', component: ActorCreate },
  //route for the about route of the web page
  { path: '/actors/update/:actorId', name: 'UpdateActor', component: ActorUpdate, props: true },

  //route for the about route of the web page
  { path: '/movies', component: MovieIndex },
  //route for the about route of the web page
  //{ path: '/movies/info/:movieId', component: MovieInfo, name: 'MovieInfo', props: true },
  //route for the about route of the web page
  { path: '/movies/create', component: MovieCreate },
  //route for the about route of the web page
  { path: '/movies/update/:movieId', name: 'UpdateMovie', component: MovieUpdate, props: true }
]

// Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes, // short for routes: routes
  mode: 'history'
})
//instatinat the vue instance
new Vue({
//define the selector for the root component
  el: '#app',
  //pass the template to the root component
  template: '<App/>',
  //declare components that the root component can access
  components: { App },
  //pass in the router to the Vue instance
  router
}).$mount('#app')//mount the router on the app